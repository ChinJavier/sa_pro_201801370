﻿**DOCUMENTO DEL ANÁLISIS Y SOLUCIÓN PROPUESTA.**  

- **Análisis y solución propuesta** 

YoVoto se perfila como una solución tecnológica que permite adaptar un enfoque de una arquitectura orientada a servicios (SOA), se busca obtener los mayores beneficios de SOA permitiendo generar una escalabilidad y autonomía de cada uno de los servicios. 

Cabe resaltar que se busca distribuir tareas y responsabilidades a cada uno de los servicios generando una reducción de costos de mantenimiento y adaptación al cambio. Para el sistema de votación, al tener un sistema bastante concurrente, es necesario  buscar el menor grado de acoplamiento y dependencia posible para que, en caso de fallar un servicio no afecte a todo el sistema en general.  

Al  ser  un  sistema  complejo,  el  implementar  SOA  facilita  la  detección  de  fallos  y mantenimiento de los servicios con el fin de mantener estable a yoVoto y así minimizar la pérdida de datos al ofrecer seguridad y alta disponibilidad al sistema de votación teniendo una infraestructura definida y una documentación común visible para todos los servicios que permita la reutilización generando reducción de costos y tiempo, algo vital para el aplicativo.  

- **Metodología** 

La  metodología  de  trabajo  propuesta  para  yoVoto  es  Scrum  permitiendo  una  buena comunicación y trabajo en equipo de forma cooperativa siendo el núcleo la transparencia y delegación de responsabilidades. 

Al ser un proyecto extenso, permite ir generando valor al negocio de forma progresiva e incremental generando un compromiso de parte del equipo. Además de dar un esquema flexible y de adaptación donde se puede replanificar durante cada iteración para lograr el objetivo final y la forma de realizar las tareas. 

- **Requerimientos** 
- Requerimientos funcionales 
  - Crear interfaz para crear elección. 
  - Crear interfaz para asignar opciones de votación. 
  - Crear módulo de enrolamiento de ciudadano. 
  - Implementar sistema de autenticación de doble factor. 
  - Crear módulo de consulta datos de ciudadanos. 
  - Realizar carga masiva de datos de ciudadanos. 
  - Autenticar al usuario votante. 
  - Crear módulo de votar para el ciudadano. 
  - Realizar carga masiva de votos presenciales. 
  - Registrar votos en el sistema. 
  - Realizar dashboard de elección en tiempo real. 
  - Cerrar votaciones automáticamente 
  - Realizar conteo de votos 
  - Implementar JOB de almacenamiento de datos. 
  - Visualizar y monitorizar el sistema. 
  - Implementar Infraestructura como Código. 
- Requerimientos no funcionales 
- Pruebas unitarias y funcionales. 
- Sistema escalable bajo demanda. 
- Encriptación de datos. 
- Sistema multiplataforma. 
- Sistema responsive. 
- Integridad de datos. 
- Documentación del sistema. 
- Validación de datos al votar. 
- Estandarizar tipo y formato de datos. 
- Validar zonas horarias. 
- **Comunicación entre los servicios.**  

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.001.jpeg)

- **Diagrama de actividades del microservicio de autenticación.**  

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.002.jpeg)

- **Diagrama de actividades de emisión de voto.**  

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.003.jpeg)

- **Diagrama de actividades del sistema de registro (enrollamiento) de ciudadanos.** 

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.004.jpeg)

- **Descripción de la seguridad de la aplicación.**  

El sistema incluye sistemas de seguridad que permiten resguardar la información de una mejor manera. De parte del backend se utiliza JWT para generar tokens del consumo de APIS con un tiempo de vida luego de realizar el proceso de Login, también se cuenta con seguridad a nivel del frontend al hacer uso de MFA lo cual da un paso extra de seguridad para ingresar a los servicios. Es importante mencionar el sistema de blockchain que permite llevar un registro transaccional inmutable. 

El flujo JWT es básicamente: 

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.005.png)

- **Descripción del uso de la tecnología blockchain.**  

Para  el  sistema  de  votaciones  permite  llevar  un  registro  transaccional  que  es  seguro, descentralizad, sincronizado y distribuido de todas las operaciones, no hay necesidad de terceros,  permite tener  un  registro  inmutable  lo  cual  ayuda  a  conservar  y proteger  la información y así evitar cualquier tipo de fraude, es un sistema distribuido. 

- **Modelo de ramas** 

El modelo de manejo de ramas en el repositorio se busca la implementación de gitflow y mantener un flujo de trabajo organizado teniendo entre las ramas principales las ramas feature, las ramas develop, reléase, hotfixes, bugfixes y la máster, donde se busque reducir la cantidad de conflictos y tener un esquema de trabajo definido. 

Las ramas feature serán de funcionalidades que se están trabajando, develop es la rama en la cual se integran los features, las reléase las que están en proceso de ir a master en las cuales pueden surgir cambios, pueden existir hotfixes para arreglar cambios en caliente de master. 

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.006.jpeg)

- **Documentación de las Pipelines para los servicios.** 

Para los microservicios se puede seguir el flujo básico de creación e instalación de paquetes y dependencias para pasar al flujo de pruebas, si es satisfactorio continua a la parte de la publicación de la nueva versión. 

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.007.png)

- **Diagrama de base de datos** 

![](Aspose.Words.7ee32602-85ed-416f-a970-5fa0eb3f1073.008.jpeg)

**Listado de Microservicios que tengan identificados. Con la descripción de los contratos.** 



|**Microservicio** |**ID** |**Ruta**  |**Método** |**Descripción** |
| - | - | - | - | - |
|<p>**Microservicio Enrolamiento** </p><p>Microservicio encargado de registrar y enrolar a los usuarios en el sistema. </p>|00-01 |/enrolar |POST |Permite registrar un usuario al sistema. |
|<p>**Microservicio Gestión Elección** </p><p>Microservicio encargado de gestionar las elecciones. </p>|00-02 |/eleccion |POST |Permite la creación de una elección tomando un título, descripción, fecha de inicio y fin de elecciones. |
||00-03 |/elección/:idEleccion/opcion |POST |Permite agregar una opción de voto a una elección específica. |
||00-04 |/elecciones |GET |Permite obtener todas las elecciones disponibles |
||00-05 |/elecciones/:idEleccion |GET |Permite obtener una elección con sus respectivas opciones. |
|**Microservicio votación** |00-06 |/realizarVoto |POST |Permite realizar un voto. |
||00-07 |/cargarVotosPresenciales |POST |Permite realizar la carga de votos presenciales. |


|Microservicio encargado de manejar las votaciones. |00-08 |/auditoria/:idEleccion |POST |Permite realizar la auditoria de votos para una elección. |
| :- | - | - | - | :- |
|<p>**Microservicio autenticación** </p><p>Microservicio encargado de autenticar a los usuarios registrados. </p>|00-09 |/validarLogin |POST |Valida si el usuario puede acceder al sistema o no. |
||00-10 |/verificarToken |POST |Valida si el token es válido |
|<p>**Microservicio datos ciudadanos (Renap)** </p><p>Microservicio encargado de proveer datos informativos de los usuarios. </p>|00-11 |/validarIdentidad |POST |Permite validar la identidad de un usuario  |
||00-12 |/validarExistenciaUsuario/:dpi |GET |Permite validar si un usuario Existe en el sistema de Renap. |
||00-13 |/validarValidezVoto/:dpi |GET |Permite si la persona puede emitir el voto o no. |
|<p>**Microservicio dashboards** </p><p>Microservicio encargado de proveer datos necesarios para los gráficos. </p>|00-14 |/dashboard/eleccion/ :idEleccion/estadística /:idEstadistica |GET |Permite obtener las estadísticas de una elección |


|**ID** 00-01 |**Microservicio:  Enrolamiento** |
| - | - |
|**Descripción**: Permite registrar un usuario al sistema. |
|**Ruta**: /enrolar |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER ||||
|PATH ||||
|BODY |CUI|Numérico  |CUI de la persona a enrolar. |
||User Agent |Numérico |Dispositivo móvil |
||teléfono |Cadena  |Teléfono de la persona. |
||fotografía |[bytes] |Fotografía de la persona. |
||fotografía del DPI |[bytes] |Fotografía del DPI |
||PIN |Numérico |PIN de acceso. |
||Correo electrónico |Email |Correo electrónico de la persona. |
||fecha y hora |DateTime |Fecha y hora de enrolamiento. |
||IP |IP |IP |
||Departamento |Cadena |Departamento de la persona. |
||Municipio |Cadena |Municipio de la persona. |
||GPS |Coordenada |Coordenadas de la persona. |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-02 |**Microservicio:  Gestión Elección** |
| - | - |
|**Descripción**: Permite la creación de una elección tomando un título, descripción, fecha de inicio y fin de elecciones. |
|**Ruta**: /eleccion |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH ||||
|BODY |TituloEleccion |Cadena |Título de la elección |
||FechaHoraInicio |DateTime |Fecha de inicio de la elección con la hora. |
||FechaHoraFin |DateTine |Fecha de fin de la elección con la hora. |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-03 |**Microservicio:  Gestión Elección** |
| - | - |
|**Descripción**: Permite agregar una opción de voto a una elección específica. |
|**Ruta**: /elección/:idEleccion/opcion |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH |idEleccion |Numérico |Id de la elección a agregar opción. |
|BODY |Opciones |[Objeto] |Metadata de la opción. |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-04 |**Microservicio: Gestión Elección** |
| - | - |
|**Descripción**: Permite obtener todas las elecciones disponibles |
|**Ruta**: /elecciones |**Método**: GET |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH ||||
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-05 |**Microservicio: Gestión Elección** |
| - | - |
|**Descripción**: Permite obtener una elección con sus respectivas opciones. |
|**Ruta**: /elecciones/:idEleccion |**Método**: GET |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH |idEleccion |Numérico |Identificador de la elección |
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-06 |**Microservicio: votación** |
| - | - |
|**Descripción**: Permite realizar un voto. |
|**Ruta**: /realizarVoto |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripción |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH ||||
|BODY |Voto |[Objetos] |Voto a guardar |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-07 |**Microservicio: votación** |
| - | - |
|**Descripción**: Permite realizar la carga de votos presenciales. |
|**Ruta**: /cargarVotosPresenciales |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH ||||
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-08 |**Microservicio: votación** |
| - | - |
|**Descripción**: Permite realizar la auditoria de votos para una elección. |
|**Ruta**: /auditoria/:idEleccion |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH |idEleccion |Numérico |Identificador de la elección. |
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-09 |**Microservicio: autenticación** |
| - | - |
|**Descripción**: Valida si el usuario puede acceder al sistema o no. |
|**Ruta**: /validarLogin |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER ||||
|PATH ||||
|BODY |DPI |Numérico |DPI de la persona que quiere acceder. |
||PIN |Numérico |PIN de acceso. |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-10 |**Microservicio: autenticación** |
| - | - |
|**Descripción**: Valida si el token es válido |
|**Ruta**: /verificarToken |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH ||||
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-11 |**Microservicio: ciudadanos (Renap)** |
| - | - |
|**Descripción**: Permite validar la identidad de un usuario |
|**Ruta**: /validarIdentidad |**Método**: POST |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER ||||
|PATH ||||
|BODY |User |Object |Usuario completo por validar. |
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-12 |**Microservicio: ciudadanos (Renap)** |
| - | - |
|**Descripción**: Permite validar si un usuario Existe en el sistema de Renap. |
|**Ruta**: /validarExistenciaUsuario/:dpi |**Método**: GET |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER ||||
|PATH |Dpi |Numérico |Dpi de la persona |
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-13 |**Microservicio: ciudadanos (Renap)** |
| - | - |
|**Descripción**: Permite si la persona puede emitir el voto o no. |
|**Ruta**: /validarValidezVoto/:dpi |**Método**: GET |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH |Dpi |Numérico |DPI de la persona. |
|BODY ||||
|OK RESPONSE |Status: 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||


|**ID** 00-14 |**Microservicio: dashboards** |
| - | - |
|**Descripción**: Permite obtener las estadísticas de una elección |
|**Ruta**: /dashboard/eleccion/ :idEleccion/estadística /:idEstadistica |**Método**: GET |**Content-Type**: JSON |
||Atributo |Tipo |Descripcion |
|HEADER |Authorization |Cadena |Token de acceso |
|PATH |idEleccion |Numérico |Identificador de la elección  |
||idEstadistica |Numérico |reporte de datos a generar |
|BODY ||||
|OK RESPONSE |Status; 200 |
|||||
|ERROR RESPONSE |Status: 400 |
|||||

